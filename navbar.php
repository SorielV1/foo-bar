<!-- navbar --> 
<nav class="navbar navbar-expand-md navbar-light bg-light">
      <a class="navbar-brand" href="index.php" style="color : #D87093;"><b>FimeBank</b></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link" href="index.html" style="color : #D87093;"><b>Inicio</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="creacuenta.php" style="color : #D87093;"><b>Crear cuenta</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="retirarefec.php" style="color : #9560a1;"><b>Retirar efectivo</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="transferirefec.php" style="color : #9560a1;"><b>Transferir efectivo</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="edocuenta.php" style="color : #9560a1;"><b>Estado cuenta</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="cancelar.php" style="color : #9560a1;"><b>Cancelar cuenta</b></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="cambiopin.php" style="color : #9560a1;"><b>Cambiar nip</b></a>
              </li>
          </div>

       </nav>