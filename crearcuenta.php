<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Crear Cuenta</title>
    <link rel="stylesheet" href="bootstra.css" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="tabla.css">
    <link rel="stylesheet" type="text/css" href="nave.css">
    <link rel="stylesheet" type="text/css" href="crearcuenta.css">
</head>

<body style="background-color : #82E0AA;">
<?php
    include $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . "config.php";
    if(isset($_POST['submit'])){
    $name=$_POST['Nombre'];
    $email=$_POST['Correo'];
    $balance=$_POST['balance'];
    $sql="insert into users(name,email,balance) values('{$name}','{$email}','{$balance}')";
    $result=mysqli_query($conn,$sql);
    if($result){
               echo "<script> alert('Genial! Cuenta creada');
                               window.location='transfermoney.php';
                     </script>";
                    
    }
  }
?>

<?php
  include $_SERVER['DOCUMENT_ROOT']. DIRECTORY_SEPARATOR . "navbar.php";
?>

        <h2 class="text-center pt-4" style="color : black;">Crear usuario </h2>
        <br>

  <div class="background">
  <div class="container">
    <div class="screen">
      <div class="screen-header">
        <div class="screen-header-right">
          <div class="screen-header-ellipsis"></div>
          <div class="screen-header-ellipsis"></div>
          <div class="screen-header-ellipsis"></div>
        </div>
      </div>
      <div class="screen-body">
        <div class="screen-body-item left">
          <img class="img-fluid" src="user3.png" style="border: none; border-radius: 50%; max-width: 200px;">
        </div>
        <div class="screen-body-item">
          <form class="app-form" method="post">
            <div class="app-form-group">
              <input class="app-form-control" placeholder="NAME" type="text" name="Nombre" required>
            </div>
            <div class="app-form-group">
              <input class="app-form-control" placeholder="EMAIL" type="email" name="Correo" required>
            </div>
            <div class="app-form-group">
              <input class="app-form-control" placeholder="BALANCE" type="number" name="balance" required>
            </div>
            <br>
            <div class="app-form-group button">
              <input class="app-form-button" type="submit" value="CREATE" name="submit"></input>
              <input class="app-form-button" type="reset" value="RESET" name="reset"></input>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<footer class="text-center mt-5 py-2">
            <p>&copy 2021. Hecho por <b>Estudiantes de ingeniería en computación inteligente</b> <br>El desarrolo de habilidades en el campo de programación web</p>
</footer>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
</body>
</html>